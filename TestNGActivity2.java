package TestNGProjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

public class TestNGActivity2 {
	@Test
	  public void testcase2()
	{  
		  WebDriver driver = new FirefoxDriver();
		  driver.get("https://alchemy.hguy.co/crm/");
		  
		  //Print the url to the console.
		  WebElement header = driver.findElement(By.linkText("SuiteCRM"));
		  System.out.println("the url is" + header.getAttribute("href"));
		  
		  
}

}
