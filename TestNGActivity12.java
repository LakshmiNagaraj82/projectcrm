package TestNGProjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class TestNGActivity12 {
  @Test
  public void testcase12() throws InterruptedException {
	  WebDriver driver = new FirefoxDriver();
	  WebDriverWait wait;
	  
 	  driver.get("https://alchemy.hguy.co/crm/");
 	  driver.manage().window().maximize();
 	  driver.findElement(By.xpath("//input[@id='user_name']")).sendKeys("admin");
 	  driver.findElement(By.xpath("//input[@id='username_password']")).sendKeys("pa$$w0rd");
 	  driver.findElement(By.xpath("//input[@id='bigbutton']")).click();
 	
 	  WebElement Activities = driver.findElement(By.xpath("//*[@id=\"grouptab_3\"]"));
 	  Activities.click();
 	  Thread.sleep(5000);
 	  WebElement Meetings = driver.findElement(By.cssSelector("#moduleTab_9_Meetings"));
      Meetings.click();
      wait = new WebDriverWait(driver, 40);
      wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[@class='module-title-text']")));
      WebElement schedulemeeting = driver.findElement(By.xpath("//div[contains(text(),'Schedule Meeting')]"));
      schedulemeeting.click();
      
      //wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[4]/div/div[1]/h2")));
      
      //ENTER DETAILS
      Thread.sleep(5000);
      WebElement subject = driver.findElement(By.id("name"));
      subject.sendKeys("projectcrm_test");
      
      //Save
      driver.findElement(By.xpath("//*[@id=\"SAVE_HEADER\"]")).click();
      
      
      //CREATE INVITE
      //WebElement addinvitee = driver.findElement(By.xpath("//button[@id='create_invitee_as_contact']"));
      //addinvitee.click();
      //WebElement first_name = driver.findElement(By.xpath("//input[@name='first_name']"));
      //first_name.sendKeys("lakshmi");
      //WebElement second_name = driver.findElement(By.xpath("//input[@name='last_name']"));
      //second_name.sendKeys("nagaraj");
      //WebElement email = driver.findElement(By.xpath("//input[@name='email1']"));
      //email.sendKeys("lakshmi.nagaraj@in.ibm.com");
      //driver.findElement(By.xpath("//button[@id='create-invitee-btn']")).click();
      
   
     
      
      
 	  
  }
}
