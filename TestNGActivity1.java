package TestNGProjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestNGActivity1 {
	@Test
	  public void testcase1()
	{  
		//Open a browser.
		  WebDriver driver = new FirefoxDriver();
		  driver.get("https://alchemy.hguy.co/crm/");
		  
		  driver.findElement(By.xpath("//input[@id='user_name']")).sendKeys("admin");
		  driver.findElement(By.xpath("//input[@id='username_password']")).sendKeys("pa$$w0rd");
		  driver.findElement(By.xpath("//input[@id='bigbutton']")).click();
		  
		//Get the title of the website.
		  System.out.println("the title of the page is" + driver.getTitle());
        Assert.assertEquals(driver.getTitle(), "SuiteCRM");
        driver.close();
}

}
