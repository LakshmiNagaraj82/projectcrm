package TestNGProjects;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
public class TestNGActivity13 {
		public WebDriverWait wait;		
		public WebDriver driver;
		@Test
		public void login() throws InterruptedException, IOException {
		 driver = new ChromeDriver();
		  driver.get("https://alchemy.hguy.co/crm/");
		  driver.manage().window().maximize();
		  driver.findElement(By.xpath("//input[@id='user_name']")).sendKeys("admin");
		  driver.findElement(By.xpath("//input[@id='username_password']")).sendKeys("pa$$w0rd");
		  driver.findElement(By.xpath("//input[@id='bigbutton']")).click();
		  WebElement all = driver.findElement(By.xpath("//*[@id=\"grouptab_5\"]"));
		  all.click();
		  driver.findElement(By.linkText("Products")).click();
		  Thread.sleep(7000);
		  driver.findElement(By.xpath("//div[contains(text(),'Create Product')]")).click();
		  List <String> list  = new ArrayList <>();//to store the excel values creating/declaring on string array here
		  FileInputStream fis = new FileInputStream("C://Users//SrilathaVeerella//Desktop//sample.xlsx"); //this excel having product name and product price
		  XSSFWorkbook wb = new XSSFWorkbook(fis); // passing workbook
		  XSSFSheet sheet1 = wb.getSheetAt(0);// getting first sheet from work book
		  int numOfCols = sheet1.getRow(0).getLastCellNum();
		 // System.out.println(numOfCols);
		  //to iterate all the columns in the excel
		  for (int j=0; j <numOfCols ; j++) { //to iterate all the columns in eachrow in the excel		 
			 String value = sheet1.getRow(0).getCell(j).getStringCellValue().toString(); //passing the each cell value to array from row 0
			 list.add(value);  //adding all the values to the list
		  }				 
          Thread.sleep(5000);
		  driver.findElement(By.id("name")).sendKeys(list.get(0)); //get(0)first value from the list
		  driver.findElement(By.id("price")).sendKeys(list.get(1));
		  driver.findElement(By.id("SAVE")).click();
		  driver.findElement(By.xpath("//div[contains(text(),'View Products')]")).click();
		//driver.close();
		}
		
}
