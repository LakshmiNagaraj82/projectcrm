package TestNGProjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

public class TestNGActivity3 {
	@Test
	  public void testcase3()
	{  
		  WebDriver driver = new FirefoxDriver();
		  driver.get("https://alchemy.hguy.co/crm/");
		  
		  //Get the first copyright text in the footer.
		  WebElement footer = driver.findElement(By.xpath("//a[@id='admin_options']"));
		  
		  //print to the console
		  System.out.println("the text on the footer is " + footer.getText());

}
}