package TestNGProjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestNGActivity4 {
  @Test
  public void testcase4() {
	  WebDriver driver = new FirefoxDriver();
	  driver.get("https://alchemy.hguy.co/crm/");
	  driver.findElement(By.xpath("//input[@id='user_name']")).sendKeys("admin");
	  driver.findElement(By.xpath("//input[@id='username_password']")).sendKeys("pa$$w0rd");
	  driver.findElement(By.xpath("//input[@id='bigbutton']")).click();
	  
	  //Verify that the homepage has opened.
	 WebElement homescreen = driver.findElement(By.xpath("//a[@class='navbar-brand with-home-icon suitepicon suitepicon-action-home']"));
	 System.out.println("the attribute is" +homescreen.getAttribute("href"));
    driver.close();
	  
  }
}
